import { createRouter, createWebHistory } from "vue-router";
import AllCountries from "../components/AllCountries.vue"

const routes = [
  {
    path: "/",
    name: "home",
    component: AllCountries,
  },
  {path: "/:code", component: () => import("../components/EachCountryData.vue")}
 
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;